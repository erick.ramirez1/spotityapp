import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from 'src/app/services/login-service.service';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit {

  cancion:any[]=[];

  constructor(private services: LoginServiceService) {
    this.services.getPlaylist().subscribe((data:any)=>{
      this.cancion=data;
      console.log(data);
    });
  }

  ngOnInit(): void {
  }

}
