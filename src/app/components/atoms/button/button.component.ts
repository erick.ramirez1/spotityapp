import { Component, OnInit, Input } from '@angular/core';
import { LoginServiceService } from 'src/app/services/login-service.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() btninput: string="";

  constructor( private service: LoginServiceService) { }

  ngOnInit(): void {
  }

  starSession() {
    this.service.starSession();
  }
}
