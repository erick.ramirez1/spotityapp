import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-btnfooter',
  templateUrl: './btnfooter.component.html',
  styleUrls: ['./btnfooter.component.scss']
})
export class BtnfooterComponent implements OnInit {

  constructor() { }

  @Input() htpps: string = "";
  @Input() btnIcon: string = "";
  @Input() paragraphs: string = "";
  
  ngOnInit(): void {
  }

}
