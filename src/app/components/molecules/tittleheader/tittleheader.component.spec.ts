import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TittleheaderComponent } from './tittleheader.component';

describe('TittleheaderComponent', () => {
  let component: TittleheaderComponent;
  let fixture: ComponentFixture<TittleheaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TittleheaderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TittleheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
