import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginServiceService } from 'src/app/services/login-service.service';
import { TokemService } from 'src/app/services/tokem.service';

@Component({
  selector: 'app-templates-home',
  templateUrl: './templates-home.component.html',
  styleUrls: ['./templates-home.component.scss']
})
export class TemplatesHomeComponent implements OnInit {

  constructor( private services: LoginServiceService, private router: Router, private tokem: TokemService ) { 
    this.obtenerUrl();
  }

  ngOnInit(): void {
  }

  obtenerUrl  () {
    const url_tokem= window.location.hash.substring(1).split("&");
    const array_tokem = url_tokem[0].split("=");
    let tokem = array_tokem[1];
    this.tokem.setTokem(tokem);
    this.router.navigate(['/home']); 
  }
}
