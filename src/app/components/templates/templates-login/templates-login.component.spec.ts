import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplatesLoginComponent } from './templates-login.component';

describe('TemplatesLoginComponent', () => {
  let component: TemplatesLoginComponent;
  let fixture: ComponentFixture<TemplatesLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TemplatesLoginComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TemplatesLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
