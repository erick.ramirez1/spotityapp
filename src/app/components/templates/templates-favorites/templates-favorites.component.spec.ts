import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplatesFavoritesComponent } from './templates-favorites.component';

describe('TemplatesFavoritesComponent', () => {
  let component: TemplatesFavoritesComponent;
  let fixture: ComponentFixture<TemplatesFavoritesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TemplatesFavoritesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TemplatesFavoritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
