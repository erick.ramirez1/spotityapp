import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TokemService } from './tokem.service';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  constructor(private http:HttpClient,private tokem:TokemService) { 
    
  }
  starSession() {
    let url= `${environment.endpoint}?client_id=${environment.clientId}&response_type=token&redirect_uri=${encodeURIComponent (environment.redirectUri)}&scope=${environment.scope.join('%20')}&show_dialog=true`;

    return window.location.href = url;
  }

  getPlaylist(){
    const headers = new HttpHeaders({
      "Authorization":"Bearer "+this.tokem.getTokem()
    });
    return this.http.get('https://api.spotify.com/v1/playlists/0t4z1E4QTmB7EWSBH7kUEh', {headers})
      .pipe( map( (data: any) =>{
        return data.tracks.items
      }
    )); 
  }

}
