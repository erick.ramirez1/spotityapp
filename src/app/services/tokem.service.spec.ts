import { TestBed } from '@angular/core/testing';

import { TokemService } from './tokem.service';

describe('TokemService', () => {
  let service: TokemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TokemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
