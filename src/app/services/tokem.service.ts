import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokemService {

  constructor() { }

  setTokem(tokem : string){
    if(tokem != undefined){
      localStorage.setItem("tokem", tokem);
    }
  }

  getTokem(){
    return window.localStorage.getItem("tokem");
  }
}
