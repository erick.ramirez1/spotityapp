import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemplatesLoginComponent } from './components/templates/templates-login/templates-login.component';
import { TemplatesHomeComponent } from './components/templates/templates-home/templates-home.component';
import { HomeComponent } from './components/pages/home/home.component';
import { LoginComponent } from './components/pages/login/login.component';
import { ButtonComponent } from './components/atoms/button/button.component';
import { FooterComponent } from './components/organisms/footer/footer.component';
import { HeaderComponent } from './components/organisms/header/header.component';
import { IconComponent } from './components/atoms/icon/icon.component';
import { BtnfooterComponent } from './components/molecules/btnfooter/btnfooter.component';
import { ParagraphsComponent } from './components/atoms/paragraphs/paragraphs.component';
import { TittleheaderComponent } from './components/molecules/tittleheader/tittleheader.component';
import { SectionComponent } from './components/organisms/section/section.component';
import { SwitchComponent } from './components/molecules/switch/switch.component';
import { FavoritesComponent } from './components/pages/favorites/favorites.component';
import { SearchComponent } from './components/pages/search/search.component';
import { TemplatesFavoritesComponent } from './components/templates/templates-favorites/templates-favorites.component';
import { TemplatesSearchComponent } from './components/templates/templates-search/templates-search.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    TemplatesLoginComponent,
    TemplatesHomeComponent,
    HomeComponent,
    LoginComponent,
    ButtonComponent,
    FooterComponent,
    HeaderComponent,
    IconComponent,
    BtnfooterComponent,
    ParagraphsComponent,
    TittleheaderComponent,
    SectionComponent,
    SwitchComponent,
    FavoritesComponent,
    SearchComponent,
    TemplatesFavoritesComponent,
    TemplatesSearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
